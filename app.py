from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, VARCHAR, NUMERIC, text, func, BIGINT, TEXT, JSON
import decimal
import json, datetime
from operator import is_not
from functools import partial
from math import radians, cos, sin, asin, sqrt
from sqlalchemy import exc
# from marshmallow_sqlalchemy import ModelSchema
from werkzeug.exceptions import HTTPException
from flask_cors import CORS

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgres://vcrdgeqhkzhspx:fbfbc4882d896ba3f8737d7282d8cce698131c8073624469df5112b3bd97e1cb@ec2-54-80-184-43.compute-1.amazonaws.com:5432/d4f4pr84d42d4b"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)
CORS(app)

# POSTGRES = {
#     'user': 'postgres',
#     'pw': 'Ayomide@96',
#     'db': 'cura',
#     'host': 'localhost',
#     'port': '5432',
# }
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
# %(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

# db = SQLAlchemy(app)
# ma = Marshmallow(app)

class PropertiesModel(db.Model):
    __tablename__ = 'properties'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    latitude = db.Column(db.NUMERIC(9, 6))
    longitude = db.Column(db.NUMERIC(9, 6))
    price = db.Column(db.Integer())
    description = db.Column(db.String())
    address = db.Column(db.String())

    def __init__(self, name, latitude, longitude, price, description, address):
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.price = price
        self.description = description
        self.address = address

    def __repr__(self):
        return f"<Property {self.name}>"
    
class BookingsModel(db.Model):
    __tablename__ = 'bookings'

    id = db.Column(db.Integer, primary_key=True)
    propertyID = db.Column(db.String())
    start = db.Column(db.String())
    end = db.Column(db.String())

    def __init__(self, propertyID, start, end):
        self.propertyID = propertyID
        self.start = start
        self.end = end

    def __repr__(self):
        return f"<Booking {self.propertyID}>"
    
class PropertySchema(ma.ModelSchema):
    class Meta:
        model = PropertiesModel
        
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)
    
# def alchemyencoder(obj):
#     """JSON encoder function for SQLAlchemy special classes."""
#     if isinstance(obj, datetime.date):
#         return obj.isoformat()
#     elif isinstance(obj, decimal.Decimal):
#         return float(obj)
    
R = 6371000 #radius of the Earth in m
def nearby(lon1, lat1, lon2, lat2):
    x = (lon2 - lon1) * cos(0.5*(lat2+lat1))
    y = (lat2 - lat1)
    return R * sqrt( x*x + y*y )
    
@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    return jsonify(error=str(e)), code

@app.route('/')
def hello():
    return {"hello": "world"}

@app.route('/bookings', methods=['POST', 'GET'])
def handle_bookings():
    if request.method == 'POST':
        if request.is_json:
            data = request.get_json()
            new_booking = BookingsModel(propertyID=data['propertyID'], start=data['start'], end=data['end'])
            db.session.add(new_booking)
            db.session.commit()
            return {"message": f"booking {new_booking.propertyID} has been created successfully."}
        else:
            return jsonify({"error": "The request payload is not in JSON format"})
            

@app.route('/property', methods=['POST', 'GET'])
def handle_properties():
    if request.method == 'POST':
        if request.is_json:
            data = request.get_json()
            new_property = PropertiesModel(name=data['name'], latitude=data['latitude'], longitude=data['longitude'], price=data['price'], description=data['description'], address=data['address'])
            db.session.add(new_property)
            db.session.commit()
            return {"message": f"property {new_property.name} has been created successfully."}
        else:
            return {"error": "The request payload is not in JSON format"}

    elif request.method == 'GET':
        properties = PropertiesModel.query.all()
        results = [
            {
                "id": propertyy.id,
                "name": propertyy.name,
                "latitude": propertyy.latitude,
                "longitude": propertyy.longitude,
                "price": propertyy.price,
                "description": propertyy.description,
                "address": propertyy.address
            } for propertyy in properties]
        
        output = json.dumps(results, sort_keys=True, cls=DecimalEncoder)
    
        j = output.replace('"[', '[').replace(']"', ']')

        js = (json.dumps(json.loads(j), indent=2))
    
        return js
    
@app.route('/properties/<property_id>/bookings', methods=['GET', 'PUT', 'DELETE'])
def handle_property(property_id):
    booking = BookingsModel.query.get_or_404(property_id)

    if request.method == 'GET':
        response = {
            "propertyID": booking.propertyID,
            "start": booking.start,
            "end": booking.end
        }
        return {"message": "success", "booking for property {response.propertyID}": response}
    
@app.route('/properties', methods=['GET'])

def distance():
    
    try:
        lat1 = float(request.args.get('LAT'))  # 28.616700
        lon1 = float(request.args.get('LONG'))  # 77.216700
    except:

        return jsonify({'message': 'Must send Latitude and Longitude'})              #77.216700
    lat = []
    lon = []
    
    properties = PropertiesModel.query.filter(PropertiesModel.latitude >= lat1, PropertiesModel.longitude <= lon1).all()
    results = [
        {
            "id": propertyy.id,
            "name": propertyy.name,
            "latitude": propertyy.latitude,
            "longitude": propertyy.longitude,
            "price": propertyy.price,
            "description": propertyy.description,
            "address": propertyy.address
        } for propertyy in properties]
    
    output = json.dumps(results, sort_keys=True, cls=DecimalEncoder)
    
    j = output.replace('"[', '[').replace(']"', ']')

    js = (json.dumps(json.loads(j), indent=2))
    
    return js
    

if __name__ == '__main__':
    app.run(debug=True)