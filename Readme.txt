# Flask App Template

**An complete Flask application template, with useful plugins.**

Use this Flask app to initiate your project with less work. In this application  template you will find the following plugins already configured:

* **Flask-Maigrate** - Flask-Migrate handles SQLAlchemy database migrations for Flask applications using Alembic.
* **Flask-CORS** - A Flask extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.
* **Flask-SQLAlchemy** - Adds SQLAlchemy support to Flask. Quick and easy.
* **Flask-Pyscog** - Add Pyscog Support PostgreSQL.
* **Flask-WTF** - Flask-Themes makes it easy for your application to support a wide range of appearances.

## Requirements

gcc, make, Python 2.5+, python-pip, virtualenv

## Instalation

First, clone this repository.

    $ git clone https://dekunle@bitbucket.org/dekunle/cura-api.git
    $ cd cura-api

Create a virtualenv, and activate this: 

    $ virtualenv env 
    $ source env/bin/activate

After, install all necessary to run:

    $ pip install -r requirements.txt

Then, run python db init and python db migrate

Than, run the application:

	$ python app.py

Or set environment for running
    Windows - set FLASK_APP=app.py
        flask run
    Linux - EXPORT FLASK_APP=app.py

To see your application, access this url in your browser: 

	http://localhost:5000

All configuration is in: `app.py`

## Endpoints
    Create a booking - http://localhost:5000/bookings
        Params - propertyID, start, end
        Method - POST

    Returns the bookings for a property - http://localhost:5000/properties/PROPERTY_ID/bookings
        Params - propertyID
        Method - GET
    
    Returns the property around Lat/Lon - http://localhost:5000/properties?at=LAT,LONG
        Params - LAT, LONG
        Method - GET